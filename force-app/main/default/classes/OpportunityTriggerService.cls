public class OpportunityTriggerService {

    public static Id renewalRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Renewal').getRecordTypeId();
    public static Id addOnRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Existing Business').getRecordTypeId();
    public static String parentHistoryRec = '<a href="/"';
    public static String parentHistoryRecBlank = '<a href="/" target="_blank"> </a>';
    public static String OPPSTAGE_CLOSEDWON = 'Closed Won';

    public static void createHistoryRecords(List<Opportunity> newOpps) {
        Set<String> parentIds = new Set<String>();
        Set<String> renIds = new Set<String>();
        Set<String> oppList = new Set<String>();
        Map<String, String> renewalsToUpdateMap = new Map<String, String>();
        List<Opportunity> renewalsToUpdate = new List<Opportunity>();

        List<Opportunity_Upsell__c> upsells = new List<Opportunity_Upsell__c>();

        List<Opportunity_Upsell__c> upsellsToInsert = new List<Opportunity_Upsell__c>();
        List<Opportunity_Upsell__c> upsellsToUpdate = new List<Opportunity_Upsell__c>();
        List<Opportunity_Upsell__c> upsellsToDelete = new List<Opportunity_Upsell__c>();

        Map<String,String> upsellsMap = new Map<String,String>();

        for(Opportunity opp : newOpps){
            if(opp.RecordTypeId == renewalRecTypeId && 
               opp.Parent_Opportunity__c != null){
                renIds.add(opp.Id);    
                parentIds.add(opp.Parent_Opportunity__c);
            }
        }

        upsells.addAll(insertParentOppToHistory(newOpps));
        upsells.addAll(insertAddOnsToHistory(newOpps, parentIds));

        if(upsells != null && !upsells.isEmpty()){
            for(Opportunity_Upsell__c upsell : upsells){
                upsellsMap.put(upsell.AddOn__c, upsell.Renewal__c);
            }
        } 

        for(Opportunity_Upsell__c historyRecCurrent : [SELECT Id, AddOn__c
                                                FROM Opportunity_Upsell__c 
                                                WHERE Renewal__c IN: renIds]){
            if(upsellsMap.containsKey(historyRecCurrent.AddOn__c)){
                upsellsToUpdate.add(historyRecCurrent);
                upsellsMap.remove(historyRecCurrent.AddOn__c);
            } else {
                upsellsToDelete.add(historyRecCurrent);
            }
        }

        for(String key : upsellsMap.keySet()){
            Opportunity_Upsell__c upsell = new Opportunity_Upsell__c();
            upsell.AddOn__c = key;
            upsell.Renewal__c = upsellsMap.get(key);
            upsellsToInsert.add(upsell);
        }
       
        system.debug('upsellsToInsert: ' + upsellsToInsert);
        system.debug('upsellsToUpdate: ' + upsellsToUpdate);
        system.debug('upsellsToDelete: ' + upsellsToDelete);

        if(upsellsToInsert != null && !upsellsToInsert.isEmpty()){
            insert upsellsToInsert;
        }

        if(upsellsToUpdate != null && !upsellsToUpdate.isEmpty()){
            update upsellsToUpdate;
        }

        if(upsellsToDelete != null && !upsellsToDelete.isEmpty()){
            delete upsellsToDelete;
        }

        if(upsellsToInsert != null && !upsellsToInsert.isEmpty() || 
           upsellsToUpdate != null && !upsellsToUpdate.isEmpty() ||
           upsellsToDelete != null && !upsellsToDelete.isEmpty()){
            calculateMRR(newOpps, renIds);
        }
    }

    public static List<Opportunity_Upsell__c> insertParentOppToHistory(List<Opportunity> newOpps) {
        Set<String> parentIds = new Set<String>();
        List<Opportunity_Upsell__c> upsells = new List<Opportunity_Upsell__c>();

        for(Opportunity opp : newOpps){
            if(opp.RecordTypeId == renewalRecTypeId && 
               opp.Parent_Opportunity__c != null){
                parentIds.add(opp.Parent_Opportunity__c);
            }
        }

        if(parentIds != null && !parentIds.isEmpty()){
            for(Opportunity oppParent : [SELECT Id, Parent_Opportunity__c, Upsell_Bypass__c, Contract_Start_Date__c, Contract_End_Date__c
                                   FROM Opportunity 
                                   WHERE Id IN: parentIds]){
                for(Opportunity oppRenewal : newOpps){
                    if(oppParent.Id == oppRenewal.Parent_Opportunity__c &&
                       oppParent.Contract_Start_Date__c >= oppRenewal.Contract_Start_Date__c &&
                       (oppParent.Contract_Start_Date__c <= oppRenewal.Contract_End_Date__c || oppParent.Contract_End_Date__c == null) &&
                       oppParent.Upsell_Bypass__c == false){
                        Opportunity_Upsell__c upsell = new Opportunity_Upsell__c();
                        upsell.AddOn__c = oppParent.Id;
                        upsell.Renewal__c = oppRenewal.Id;
                        upsells.add(upsell);
                    }
                }
            }
        }
        
        return upsells;
    }

    public static List<Opportunity_Upsell__c> insertAddOnsToHistory(List<Opportunity> newOpps, Set<String> parentIds) {
        List<Opportunity_Upsell__c> upsells = new List<Opportunity_Upsell__c>();

        for(Opportunity oppAddOn : [SELECT Id, Parent_Opportunity__c, Upsell_Bypass__c, Contract_Start_Date__c, Contract_End_Date__c
                                    FROM Opportunity 
                                    WHERE RecordTypeId =: addOnRecTypeId AND 
                                          Parent_Opportunity__c IN :parentIds AND
                                          Upsell_Bypass__c = false AND
                                          StageName =: OPPSTAGE_CLOSEDWON]){
            for(Opportunity oppNew : newOpps){
                if(oppNew.Parent_Opportunity__c == oppAddOn.Parent_Opportunity__c &&
                   oppNew.RecordTypeId == renewalRecTypeId &&
                   oppNew.Parent_Opportunity__c != null &&
                   oppAddOn.Contract_Start_Date__c >= oppNew.Contract_Start_Date__c &&
                   (oppAddOn.Contract_Start_Date__c <= oppNew.Contract_End_Date__c || oppNew.Contract_End_Date__c == null) &&
                   oppAddOn.Upsell_Bypass__c == false){
                    Opportunity_Upsell__c upsell = new Opportunity_Upsell__c();
                    upsell.AddOn__c = oppAddOn.Id;
                    upsell.Renewal__c = oppNew.Id;
                    upsells.add(upsell);    
                }
            }
        }

        return upsells;
    }

    public static void calculateMRR(List<Opportunity> newOpps, Set<String> renIds) {

        List<Opportunity> oppList = new List<Opportunity>();
        List<Opportunity_Upsell__c> upsells = new List<Opportunity_Upsell__c>();

        for(Opportunity_Upsell__c historyRec : [SELECT MRR_Omnidocs__c, MRR_Templafy__c, Renewal__c, Parent__c
                                                FROM Opportunity_Upsell__c 
                                                WHERE Renewal__c IN: renIds]){
            upsells.add(historyRec);
        }

        for(Opportunity opp : newOpps){
            Decimal addOnsMRROmnidocs = 0;
            Decimal addOnsMRRTemplafy = 0;
            for(Opportunity_Upsell__c historyRec : upsells){
                if(historyRec.Renewal__c == opp.Id && !historyRec.Parent__c.startsWith(parentHistoryRec)){
                    addOnsMRROmnidocs += historyRec.MRR_Omnidocs__c;
                    addOnsMRRTemplafy += historyRec.MRR_Templafy__c;
                }
            }
            oppList.add(new Opportunity(Id = opp.Id, 
                                        MRR_Omnidocs_add_ons__c = addOnsMRROmnidocs, 
                                        MRR_Templafy_add_ons__c = addOnsMRRTemplafy));
        }

        if(oppList != null && !oppList.isEmpty()){
            update oppList;
        }
    }

    public static void updateOppHierarchy(List<Opportunity> oldOpps) {
        Set<String> parents = new Set<String>();
        List<Opportunity> oppList = new List<Opportunity>();
        
        for(Opportunity opp : oldOpps){
            if(opp.RecordTypeId == addOnRecTypeId){
                parents.add(opp.Parent_Opportunity__c);
            }
        }

        if(parents != null && !parents.isEmpty()){
            for(Opportunity oppRens : [SELECT Id, Parent_Opportunity__c
                                       FROM Opportunity 
                                       WHERE RecordTypeId =: renewalRecTypeId AND 
                                             Parent_Opportunity__c IN :parents]){
                oppList.add(new Opportunity(Id = oppRens.Id));
            }
            update oppList;
        }
        
    }

    public static void deleteOppHistoryRecs(List<Opportunity> oldOpps) {
        Set<String> opps = new Set<String>();
        List<Opportunity_Upsell__c> upsellsToDelete = new List<Opportunity_Upsell__c>();

        for(Opportunity opp : oldOpps){
            opps.add(opp.Id);
        }    

        for(Opportunity_Upsell__c historyRec : [SELECT MRR_Omnidocs__c, MRR_Templafy__c, Renewal__c, Parent__c, AddOn__c
                                                    FROM Opportunity_Upsell__c 
                                                    WHERE AddOn__c IN: opps]){
                upsellsToDelete.add(historyRec);
        }

        if(upsellsToDelete != null && !upsellsToDelete.isEmpty()){
            delete upsellsToDelete;
        }
    }
}