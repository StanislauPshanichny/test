public class OpportunityTriggerHandler {

    public static void onAfterInsert(List<Opportunity> newOpps) {
        OpportunityTriggerService.createHistoryRecords(newOpps);
    }

    public static void onAfterUpdate(List<Opportunity> newOpps) {
        if(checkRecursive.runOnce()){
            OpportunityTriggerService.createHistoryRecords(newOpps);
        }
    }

    public static void onAfterDelete(List<Opportunity> oldOpps) {
        OpportunityTriggerService.updateOppHierarchy(oldOpps);
    }

    public static void onBeforeDelete(List<Opportunity> oldOpps) {
        OpportunityTriggerService.deleteOppHistoryRecs(oldOpps);
    }
}