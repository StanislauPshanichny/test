trigger OpportunityTrigger on Opportunity (after insert, after update, before delete, after delete) {
    
    if (Trigger.isAfter && Trigger.isInsert) {
        OpportunityTriggerHandler.onAfterInsert(Trigger.new);
    }

    if (Trigger.isAfter && Trigger.isUpdate) {
        OpportunityTriggerHandler.onAfterUpdate(Trigger.new);
    }

    if (Trigger.isAfter && Trigger.isDelete) {
        OpportunityTriggerHandler.onAfterDelete(Trigger.old);
    }

    if (Trigger.isBefore && Trigger.isDelete) {
        OpportunityTriggerHandler.onBeforeDelete(Trigger.old);
    }
}